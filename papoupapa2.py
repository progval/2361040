# -*- coding: utf8 -*-

# modes : (l'ordre est important)
PAPOU = 0
A_POUX = 1
PAPA = 2

HOMME = 10
POU = 11

def papoupapa(separateurs, prefixe=u'les papous', objet=HOMME, mode=A_POUX, pluriel=False):
    mode_suivant = mode + 1
    if mode_suivant == 3:
        mode_suivant = 0
        separateurs = separateurs[0:-1]
    suffixe = 's' if pluriel else ''
    if len(separateurs) == 0:
        return []
    elif objet == POU and mode == A_POUX: # Un pou n'a pas de poux
        return papoupapa(separateurs, prefixe, objet, mode_suivant, pluriel)
    else:
        chaines = []
        if mode == A_POUX:
            chaines.append(u' pas à poux')
            chaines.extend([(u' à poux' + x) for x in
                papoupapa(separateurs, prefixe + u' à poux', POU, mode_suivant, True)])
        elif mode == PAPA:
            chaines.append(u' pas papa' + suffixe)
            chaines.extend([u' papa' + suffixe + ' d\'un' + x for x in
                papoupapa(separateurs, prefixe + u' papa', objet, mode_suivant, True)])
        elif mode == PAPOU:
            chaines.append(u' pas papou' + suffixe)
            chaines.append(u' papou' + suffixe)

        resultats = []
        for chaine in chaines:
            souschaines = papoupapa(separateurs[0:-1], prefixe=prefixe+chaine,
                objet=objet, mode=mode_suivant, pluriel=pluriel)
            if souschaines == []:
                resultats.append(chaine)
            else:
                resultats.extend([chaine + x for x in souschaines])

        return resultats

if __name__ == '__main__':
    print 'En Papouasie, il y a les papous' + (', les papous'.join(papoupapa('\n/.;,'))) + '.'
