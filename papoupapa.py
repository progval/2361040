# -*- coding: utf8 -*-

def papoupapa(level, peut_etre_non_papou=False, peut_etre_pas_papa=True, est_un_pou=False, papa_connu=False, pluriel=False):
    if level == 0:
        tree = {}
    else:
        tree = {}
        suffixe = 's' if pluriel else ''
        if peut_etre_non_papou:
            tree.update({u'papou' + suffixe: {}, u'pas papou' + suffixe: {}})
        if not est_un_pou:
            tree.update({u'à poux': papoupapa(level-1, peut_etre_non_papou=True, est_un_pou=True, pluriel=True), u'pas à poux': {}})
        if peut_etre_pas_papa:
            for type_ in ('papou', 'pas papou'):
                tree.update({u'papa d\'un' + (' pou' if est_un_pou else ''): papoupapa(level-1, peut_etre_non_papou=True, est_un_pou=est_un_pou, papa_connu=True, pluriel=pluriel), u'pas papa' + suffixe: {}})
        if level > 1 and not papa_connu:
            tree.update({u'à papa' + suffixe: papoupapa(level-1, peut_etre_non_papou=True, peut_etre_pas_papa=False, est_un_pou=est_un_pou, pluriel=pluriel)})
    return tree

def to_string(tree, concatener=True):
    if tree == {}:
        return ''
    else:
        strings = []
        for key, subtree in tree.items():
            subtree = to_string(subtree)
            strings.append(key + subtree)
        if concatener:
            return ' ' + (' '.join(strings))
        else:
            return strings

if __name__ == '__main__':
    tree = papoupapa(5)
    print u'En Papouasie, il y a les papous ' + (u', les papous '.join(to_string(tree, False))) + '.'
